<?php
// HTTP
define('HTTP_SERVER', 'http://mastera/');

// HTTPS
define('HTTPS_SERVER', 'https://mastera/');

// DIR
define('DIR_APPLICATION', 'catalog/');
define('DIR_SYSTEM', 'system/');
define('DIR_IMAGE', 'image/');
define('DIR_LANGUAGE', 'catalog/language/');
define('DIR_TEMPLATE', 'catalog/view/theme/');
define('DIR_CONFIG', 'system/config/');
define('DIR_CACHE', 'system/storage/cache/');
define('DIR_DOWNLOAD', 'system/storage/download/');
define('DIR_LOGS', 'system/storage/logs/');
define('DIR_MODIFICATION', 'system/storage/modification/');
define('DIR_UPLOAD', 'system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
//define('DB_USERNAME', 'u66000yx_test22');
//define('DB_PASSWORD', '123456');

define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');

//define('DB_USERNAME', 'homestead');
//define('DB_PASSWORD', 'secret');

define('DB_DATABASE', 'u66000yx_test22');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');