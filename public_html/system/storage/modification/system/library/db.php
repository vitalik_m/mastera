<?php
class DB {
	private $adaptor;

	public function __construct($adaptor, $hostname, $username, $password, $database, $port = NULL) {
		$class = 'DB\\' . $adaptor;

		if (class_exists($class)) {
			$this->adaptor = new $class($hostname, $username, $password, $database, $port);
		} else {
			throw new \Exception('Error: Could not load database adaptor ' . $adaptor . '!');
		}
	}

	public function query($sql, $params = array()) {
					
		$min_time = 5;//МИНИМАЛЬНОЕ ВРЕМЯ ИСПОЛНЕНИЯ ЗАПРОСА В МС, ЗАПИСЫВАЕМОЕ В ЛОГ
        $file = debug_backtrace();
        $name = (!isset($file[0]['file'])) ? 'N/A' : $file[0]['file'];
        $start = (time() + microtime());
        $query_out = $this->adaptor->query($sql, $params);
        $end = (time() + microtime());
        $sql_time = round($end - $start, 5)*1000;
        if ($sql_time > $min_time) {
        file_put_contents(DIR_LOGS.'sql_time.log', ("\nСтраница:".$_SERVER['REQUEST_URI']."\nИсточник:" . $name . "\nВремя выполнения: " . $sql_time . "мс \n\n" . $sql . "\n\n----------------------\n"), FILE_APPEND); }
        return $query_out;
			
	}

	public function escape($value) {
		return $this->adaptor->escape($value);
	}

	public function countAffected() {
		return $this->adaptor->countAffected();
	}

	public function getLastId() {
		return $this->adaptor->getLastId();
	}
	
	public function connected() {
		return $this->adaptor->connected();
	}
}