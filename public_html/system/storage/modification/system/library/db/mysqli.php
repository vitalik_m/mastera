<?php
namespace DB;
final class MySQLi {
	private $connection;

	public function __construct($hostname, $username, $password, $database, $port = '3306') {
		$this->connection = new \mysqli($hostname, $username, $password, $database, $port);

		if ($this->connection->connect_error) {
			throw new \Exception('Error: ' . $this->connection->error . '<br />Error No: ' . $this->connection->errno);
		}

		$this->connection->set_charset("utf8");
		$this->connection->query("SET SQL_MODE = ''");
	}

	public function query($sql) {
					
		$min_time = 5;//МИНИМАЛЬНОЕ ВРЕМЯ ИСПОЛНЕНИЯ ЗАПРОСА В МС, ЗАПИСЫВАЕМОЕ В ЛОГ
        $file = debug_backtrace();
        $name = (!isset($file[0]['file'])) ? 'N/A' : $file[0]['file'];
        $start = (time() + microtime());
        $query_out = $this->connection->query($sql);
        $end = (time() + microtime());
        $sql_time = round($end - $start, 5)*1000;
        if ($sql_time > $min_time) {
        file_put_contents(DIR_LOGS.'sql_time.log', ("\nСтраница:".$_SERVER['REQUEST_URI']."\nИсточник:" . $name . "\nВремя выполнения: " . $sql_time . "мс \n\n" . $sql . "\n\n----------------------\n"), FILE_APPEND); }
		$query = $query_out;
			

		if (!$this->connection->errno) {
			if ($query instanceof \mysqli_result) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new \stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				$query->close();

				return $result;
			} else {
				return true;
			}
		} else {
			throw new \Exception('Error: ' . $this->connection->error  . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
		}
	}

	public function escape($value) {
		return $this->connection->real_escape_string($value);
	}
	
	public function countAffected() {
		return $this->connection->affected_rows;
	}

	public function getLastId() {
		return $this->connection->insert_id;
	}
	
	public function connected() {
		return $this->connection->ping();
	}
	
	public function __destruct() {
		$this->connection->close();
	}
}
